# LNLP-Toolkit

This toolkit ais to assemble the whole tools and libraries available for NLP pre-processing.

## Installation
Automatic installation with the script: 
> install.sh [-h] [-g] [-p PREFIX] <br>
>&nbsp;&nbsp;&nbsp;&nbsp;-h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;help <br>
>&nbsp;&nbsp;&nbsp;&nbsp;-p PREFIX&nbsp;&nbsp;&nbsp;&nbsp;specify a prefix (default /usr/local/) <br>



# Tools available
## ltokenizer
The very first step for every NLP process is a tokenizer: a tool which segement text into tokens. These tokens  can be words or set of words. For example, "c'est-à-dire" or "aujourd'hui" are a tokens. The tokenizations rules are available for every european languages, but specific rules have been written for French and English.

Full support for european (latin to non-latin languages), asian (chinese, japanese, ...) , arabic (all variant), and many other languages.

Included stemming and stopwords for several languages.

### lStemmer
For many users, one needs a stemmer for Information Retreival. As State-of-the-Art stemmer, we included the Snowball Stemmer, for all language available.
#### References
http://snowballstem.org/

### lStopWords
In Information Retrieval, it is common to use stop-words list. This is now part of the lnlp-toolkit, for French, English, Portugese, Italian, Spanish and Dutch. The list comes from the website https://www.ranks.nl



## lfiltering
Enable to filter bitext* according the sentence length, the sentence length ratio and other tricks.
> *bitext: two bilingual corpora aligned sentence by sentence, the second one is the translation of the first one. They are used as training corpora for Neural Machine Translation
### TODO
Add specific processing for arabic languages.

### References
> [Zalmout, 2017]	Nasser Zalmout and Nizar Habash: "Optimizing Tokenization Choice for Machine Translation across Multiple Target Languages", In The Prague Bulletin of Mathematical Linguistics, Vol. 108, June 2017.
>

# Python package

All previously listed tools are available via python bindings.

#Build/Install
To build and install these bindings:
```bash
cd python
python3 setup.py install
```

`cython` and all the dependencies of the CPP library are needed to build the python bindings.

#Pip install
This is also available in the Pypi package registry of the repository on Gitlab, the documentation to install it can be found in the sub-directory python.

### References
https://www.ranks.nl
### TODO
Add other european languages.

