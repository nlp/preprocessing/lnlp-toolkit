#include "zh_tokenizer.h"

using namespace lnlp;

bool Tokenizer_zh::process_lang (vector<wstring>& vecwtoken)
{
    auto vecwtoken_it=vecwtoken.begin();
    auto vecwtoken_it_prev=vecwtoken.begin();
    if (vecwtoken_it != vecwtoken.end()) vecwtoken_it++; else return false;
    wstring toTest;
    bool cot_open=false;
    while (vecwtoken_it != vecwtoken.end() )
    {
        toTest=(*vecwtoken_it_prev)+(*vecwtoken_it);
        if (std::find(wlang_process.begin(), wlang_process.end(), toTest) != wlang_process.end()) // && (*vecwtoken_it) == L"."
        {
            (*vecwtoken_it_prev)=toTest;
            (*vecwtoken_it)=L"";
            return true;
        }
        if (std::find(wlang_process_not.begin(), wlang_process_not.end(), toTest) != wlang_process_not.end() && ((int)(*vecwtoken_it).size()>0 && (*vecwtoken_it)[0]==u'\'')) // && (*vecwtoken_it) == L"."
        {
            (*vecwtoken_it_prev)=toTest.substr(0,toTest.find(u'n'));
            (*vecwtoken_it)=L"n't";
            return true;
        }
//         if (((int)(*vecwtoken_it).size()== 1 && (*vecwtoken_it)[0]==u'\'') && (int)(*vecwtoken_it_prev).size() > 0 && ! cot_open)
//         {
//             if ((*vecwtoken_it_prev)[(int)(*vecwtoken_it_prev).size()-1] == 's')
//             {
//                 (*vecwtoken_it_prev)=toTest;
//                 (*vecwtoken_it)=L"";
//                 return true;
//             }
//         }
//         if (((int)(*vecwtoken_it).size()== 1 && (*vecwtoken_it)[0]==u'\''))
//         {
//             if (cot_open) cot_open=false;
//             else cot_open=true;
//         }
        vecwtoken_it++;
        vecwtoken_it_prev++;
    }
    return false;
}


vector<string> Tokenizer_zh::tokenize(string& str)
{
    vector<string> to_return;
    vector<unsigned short> utf16str;
    vector<unsigned int> utf32str;
    vector<wstring> to_return_wchar;
    wstring wtoken;
    bool process_numbers_todo=false;
    utf8::utf8to16(str.begin(), str.end(), back_inserter(utf16str));
    auto utf16str_it=utf16str.begin();
    while (utf16str_it != utf16str.end())
    {
        unsigned short wc=wtoken[0];
        unsigned short cwc=(*utf16str_it);
        if (cwc == 160)  cwc=32;
        if (cwc == u'´') cwc=u'\'';
        if (cwc == u'’') cwc=u'\'';
        if (cwc >= 19968)
        {
            if ((int)wtoken.size() > 0)
            to_return_wchar.push_back(wtoken);
            wtoken.clear();
        }
        if (seps(wc))
        {
            if ((int)wtoken.size() > 0)
            to_return_wchar.push_back(wtoken);
            wtoken.clear();
        }
        if (seps(cwc))
        {
            if ((int)wtoken.size() > 0)
            to_return_wchar.push_back(wtoken);
            wtoken.clear();
            
        }
        if (cwc > 32 || cwc == 10) wtoken.push_back(cwc);
        else
        {
            if ((int)wtoken.size() > 0)
            to_return_wchar.push_back(wtoken);
            wtoken.clear();
            to_return_wchar.push_back(L"@@@");            
        }
        if (cwc > 47 && cwc < 58 ) process_numbers_todo=true;
        
        utf16str_it++;
    }
    if ((int)wtoken.size() > 0) to_return_wchar.push_back(wtoken);
    wtoken.clear();
    while (process_cots(to_return_wchar) )
    {
        continue;
    }
    if (process_numbers_todo)
    {
        while (process_numbers(to_return_wchar) )
        {
            continue;
        }
    }
    
    while (process_dots(to_return_wchar))
    {
        to_return_wchar=clean_vector(to_return_wchar);
    }
    process_final_dots(to_return_wchar);
    while (process_lang(to_return_wchar))
    {
        to_return_wchar=clean_vector(to_return_wchar);
    }
    if (lowercased) process_lowercase(to_return_wchar);
    if (no_punct) process_no_punct(to_return_wchar);
    auto to_return_wchar_it=to_return_wchar.begin();
    while (to_return_wchar_it != to_return_wchar.end())
    {
        string utf8str;
        wstring utf16str=(*to_return_wchar_it);
        utf8::utf16to8(utf16str.begin(), utf16str.end(), back_inserter(utf8str));
        if ((int)utf8str.size() > 0 && ! is_separator(utf8str)) to_return.push_back(utf8str);
        to_return_wchar_it++;
    }
    return to_return;
}
