PROJECT(lnlp)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_C_FLAGS " -O3 -fPIC -std=gnu++11")
include_directories(BEFORE ${CMAKE_CURRENT_SOURCE_DIR}/src)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
link_directories(/usr/local/lib)
SET(NLP_SRCS
    main_tokenizer.cpp
)

SET(FILTERING_SRCS
    main_filtering.cpp
)

SET(FILTERING_MONO_SRCS
    main_filtering_mono.cpp
)


add_executable(ltokenizer ${NLP_SRCS})
add_executable(lfiltering ${FILTERING_SRCS})
add_executable(lfiltering-mono ${FILTERING_MONO_SRCS})

target_link_libraries(ltokenizer boost_regex snowball lnlp)
target_link_libraries(lfiltering boost_regex lnlp)
target_link_libraries(lfiltering-mono boost_regex lnlp)
install(TARGETS ltokenizer lfiltering lfiltering-mono DESTINATION bin)
