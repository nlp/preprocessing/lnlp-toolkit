import pylnlp
from pylnlp import Tokenizer

qtok = Tokenizer("fr")
str_test = "Arc de triomphe de l'Etoile, Après-Midi"
print("Original:\t\t"+str_test)
qtok.set_lowercased(True)
print("Lowercased True:\t" + qtok.tokenize(str_test))
print("arc de triomphe de l 'etoile , après-midi" == qtok.tokenize(str_test))
qtok.set_lowercased(False)
print("Lowercased False:\t"+ qtok.tokenize(str_test))
print("Arc de triomphe de l' Etoile , Après-Midi" == qtok.tokenize(str_test))

