#!/bin/bash

mkdir build -pv 
pushd build
	cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/Tools/lnlp-toolkit
	make -j4 && make install
popd

