# distutils: language = c++
# cython : language_level = 3

from libcpp cimport bool
from libcpp.string cimport string
from libcpp.vector cimport vector

from pylnlp.includes.tokenizer cimport CTokenizer, CTokenizerFr, CTokenizerEn, CTokenizerChar, CTokenizerZh

cdef class Tokenizer:
    cdef CTokenizer *c_tok
    def __cinit__(self, lang, lowercased=False, underscore=False, dash=False, aggressive=False, noPunct=False):
        if lang == "fr":
            self.c_tok = new CTokenizerFr(lowercased, underscore, dash, aggressive, noPunct)
        elif lang == "en":
            self.c_tok = new CTokenizerEn(lowercased, underscore, dash, aggressive, noPunct)
        elif lang == "char":
            self.c_tok = new CTokenizerChar(lowercased, underscore, dash, aggressive, noPunct)
        elif lang == "zh":
            self.c_tok = new CTokenizerZh(lowercased, underscore, dash, aggressive, noPunct)
        else:
            self.c_tok = new CTokenizer(lowercased, underscore, dash, aggressive, noPunct)

    def __dealloc__(self):
        del self.c_tok

    def tokenize(self, sentence: str):
        return self.c_tok.tokenize_sentence_to_string(self._str_to_bytes(sentence)).decode()

    def detokenize(self, sentence: str):
        return self.c_tok.detokenize_sentence_to_string(self._str_to_bytes(sentence)).decode()

    def tokenize_str_to_list(self, sentence: str):
        cdef vector[string] ret = self.c_tok.tokenize_sentence(self._str_to_bytes(sentence))
        return [c_sentence.decode() for c_sentence in ret]

    def _normalize_one(self, sentence: str):
        return self.c_tok.normalize(self._str_to_bytes(sentence)).decode()

    def normalize(self, sentences):
        if isinstance(sentences, str):
            return self._normalize_one(sentences)
        return [self._normalize_one(sentence) for sentence in sentences]

    def set_param(self, lowercased: bool, underscore: bool, dash: bool, aggressive: bool, noPunct: bool):
        self.c_tok.setParam(lowercased, underscore, dash, aggressive, noPunct)

    def set_lowercased(self, lowercased: bool):
        self.c_tok.setLowercased(lowercased)

    def set_underscore(self, underscore: bool):
        self.c_tok.setUnderscore(underscore)

    def set_dash(self, dash: bool):
        self.c_tok.setDash(dash)

    def set_aggressive(self, aggressive: bool):
        self.c_tok.setAggressive(aggressive)

    def set_no_punct(self, noPunct: bool):
        self.c_tok.setNoPunct(noPunct)

    def print_param(self):
        self.c_tok.printParam()

    def get_lang(self):
        return self.c_tok.getlang().decode()

    @staticmethod
    def _str_to_bytes(string):
        return string.encode("utf-8")

cdef class TokenizerFr(Tokenizer):

    def __cinit__(self, lowercased=False, underscore=False, dash=False, aggressive=False, noPunct=False):
        self.c_tok = new CTokenizerFr(lowercased, underscore, dash, aggressive, noPunct)

cdef class TokenizerEn(Tokenizer):

    def __cinit__(self, lowercased=False, underscore=False, dash=False, aggressive=False, noPunct=False):
        self.c_tok = new CTokenizerEn(lowercased, underscore, dash, aggressive, noPunct)

cdef class TokenizerChar(Tokenizer):

    def __cinit__(self, lowercased=False, underscore=False, dash=False, aggressive=False, noPunct=False):
        self.c_tok = new CTokenizerChar(lowercased, underscore, dash, aggressive, noPunct)

cdef class TokenizerZh(Tokenizer):

    def __cinit__(self, lowercased=False, underscore=False, dash=False, aggressive=False, noPunct=False):
        self.c_tok = new CTokenizerZh(lowercased, underscore, dash, aggressive, noPunct)
