# distutils: language = c++
# cython : language_level = 3

from libcpp.string cimport string
from libcpp.vector cimport vector

from pylnlp.includes.stemmer cimport CStemmer

cdef class Stemmer:
    cdef CStemmer *c_stem

    def __cinit__(self, lang: str):
        self.c_stem = new CStemmer(self._str_to_bytes(lang))

    def __dealloc__(self):
        del self.c_stem

    def stem(self, word: str):
        return self.c_stem.stem(self._str_to_bytes(word)).decode()

    def stem_sentence(self, sentence: str):
        return self.c_stem.stem(self._str_to_bytes(sentence)).decode()

    def stem_sentence_vector(self, sentences: [str]):
        cdef vector[string] vect = [self._str_to_bytes(sentence) for sentence in sentences]
        cdef vector[string] ret = self.c_stem.stem_sentence_vector(vect)
        return [sentence.decode() for sentence in ret]

    @staticmethod
    def _str_to_bytes(string):
        return string.encode("utf-8")
