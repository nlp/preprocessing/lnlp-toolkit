# distutils: language = c++
# cython : language_level = 3

from libcpp.string cimport string
from libcpp.vector cimport vector

from pylnlp.includes.stopword cimport CStopwords

cdef class Stopwords:
    cdef CStopwords c_stopwords

    def checkword(self, token: str, lang: str):
        c_token = self._str_to_bytes(token)
        c_lang = self._str_to_bytes(lang)
        return self.c_stopwords.checkword(c_token, c_lang)

    def filter_stopwords(self, sentence: [str], lang: str):
        c_lang = self._str_to_bytes(lang)
        cdef vector[string] c_sentence = [self._str_to_bytes(word) for word in sentence]
        cdef vector[string] ret = self.c_stopwords.filter_stopwords(c_sentence, c_lang)
        return [word.decode() for word in ret]

    @staticmethod
    def _str_to_bytes(string):
        return string.encode("utf-8")
