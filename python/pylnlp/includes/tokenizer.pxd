# distutils: language = c++

from libcpp cimport bool
from libcpp.string cimport string
from libcpp.vector cimport vector

cdef extern from "tokenizer.h" namespace "lnlp":
    cdef cppclass CTokenizer "lnlp::Tokenizer":
        CTokenizer(bool, bool, bool, bool, bool)
        string tokenize_sentence_to_string(string &)
        string detokenize_sentence_to_string(string &)
        vector[string] tokenize_sentence(string &)
        string normalize(string &)
        void setParam(bool, bool, bool, bool, bool)
        void setLowercased(bool)
        void setUnderscore(bool)
        void setDash(bool)
        void setAggressive(bool)
        void setNoPunct(bool)
        void setSyntax(int)
        void setSyntaxFlag(string&)
        string getlang()
        string printParam()

cdef extern from "fr_tokenizer.h" namespace "lnlp":
    cdef cppclass CTokenizerFr "lnlp::Tokenizer_fr" (CTokenizer):
        CTokenizerFr(bool, bool, bool, bool, bool)

cdef extern from "en_tokenizer.h" namespace "lnlp":
    cdef cppclass CTokenizerEn "lnlp::Tokenizer_en" (CTokenizer):
        CTokenizerEn(bool, bool, bool, bool, bool)

cdef extern from "char_tokenizer.h" namespace "lnlp":
    cdef cppclass CTokenizerChar "lnlp::Tokenizer_char" (CTokenizer):
        CTokenizerChar(bool, bool, bool, bool, bool)

cdef extern from "zh_tokenizer.h" namespace "lnlp":
    cdef cppclass CTokenizerZh "lnlp::Tokenizer_zh" (CTokenizer):
        CTokenizerZh(bool, bool, bool, bool, bool)
