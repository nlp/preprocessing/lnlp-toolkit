# distutils: language = c++

from libcpp.string cimport string
from libcpp.vector cimport vector

cdef extern from "stemmer.h" namespace "lnlp":
    cdef cppclass CStemmer "lnlp::Stemmer":
        CStemmer (const char*)
        string stem(string&)
        string stem_sentence(string&)
        vector[string] stem_sentence_vector(vector[string]&)
