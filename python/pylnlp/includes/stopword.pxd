# distutils: language = c++

from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport bool

cdef extern from "stopwords.h" namespace "lnlp":
    cdef cppclass CStopwords "lnlp::Stopwords":
        CStopwords()
        bool checkword(string&, string)
        vector[string] filter_stopwords(vector[string]&, string)
