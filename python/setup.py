import os
from os.path import join as pjoin
import pathlib
import shutil

from setuptools import Extension, setup
from Cython.Distutils import build_ext as _build_ext
from Cython.Build import cythonize


NAME = "pylnlp"
SRC_DIR = "pylnlp"
VERSION = os.getenv("VERSION_TAG", "1.0.0")

class build_ext(_build_ext):
    def run(self):
        self._build_cmake()

        self.announce("CMake build done")

        super().run()

    def _build_cmake(self):
        cwd = pathlib.Path().absolute()
        parent = cwd.parent
        os.makedirs(self.build_temp, exist_ok=True)

        os.chdir(self.build_temp)

        self.spawn(["cmake", str(parent)])
        self.spawn(["make", "-j", "8"])

        os.chdir(str(cwd))

        build_lib_dir = pjoin(self.build_lib, NAME)

        self.move_file(pjoin(self.build_temp,"liblnlp.a"), "build")
        self.move_file(pjoin(self.build_temp, "externals", "snowball", "libsnowball.a"), "build")


ext_modules = cythonize(Extension(
                    SRC_DIR + ".*",
                    sources=[SRC_DIR + "/*.pyx"],
                    include_dirs=["../include", "../externals/snowball/include"],
                    languages='c++',
                    extra_objects=["build/liblnlp.a", "build/libsnowball.a"]

              ))

setup(
        name=NAME,
        version=VERSION,
        packages=[SRC_DIR],
        package_data={SRC_DIR: ['*.pxd', '*.pyx', 'includes/*.pxd']},
        data_files=[],

        cmdclass={
            "build_ext": build_ext,
            },
        ext_modules = ext_modules,

        setup_requires='cython >= 0.29',
        python_requires='>=3.6',

)
