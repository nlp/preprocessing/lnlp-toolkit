#ifndef LNLP_ZH_TOKENIZER_H
#define LNLP_ZH_TOKENIZER_H

#include "tokenizer.h"

namespace lnlp {
    
    class Tokenizer_zh: public Tokenizer {
        public:
          
            Tokenizer_zh (bool lowercased=true, bool underscore=true, bool dash=true, bool aggressive=true, bool nopunct=true):
                Tokenizer (lowercased, underscore, dash, aggressive, nopunct)
                {
                    lang="zh";
                    wlang_process = {L"'s", L"'t", L"'ll", L"'roll", L"'n'roll", L"n'roll", L"rock'n'roll", L"n'b",  L"n'B", L"'n'b",  L"'n'B", L"r'n'b", L"R'n'B", L"'m"};     
                    wlang_process_not = {L"don't", L"isn't", L"didn't"};     
                };
        protected:
            bool process_lang(vector<wstring> & vecwtoken) override;
            vector<string> tokenize(string& str) override;
    };
}

#endif // LNLP_ZH_TOKENIZER_H
