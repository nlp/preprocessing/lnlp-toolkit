#ifndef LNLP_EN_TOKENIZER_H
#define LNLP_EN_TOKENIZER_H

#include "tokenizer.h"

namespace lnlp {
    
    class Tokenizer_en: public Tokenizer {
        public:
          
//            Tokenizer_en (int syntax=PLAIN, bool lowercased=true, bool underscore=true, bool dash=true, bool aggressive=true, bool nopunct=true):
//                Tokenizer (syntax, lowercased, underscore, dash, aggressive, nopunct){lang="en";};
            Tokenizer_en (bool lowercased=true, bool underscore=true, bool dash=true, bool aggressive=true, bool nopunct=true):
                Tokenizer (lowercased, underscore, dash, aggressive, nopunct)
                {
                    lang="en";
                    wlang_process = {L"'s", L"'t", L"'ll", L"'roll", L"'n'roll", L"n'roll", L"rock'n'roll", L"n'b",  L"n'B", L"'n'b",  L"'n'B", L"r'n'b",  L"'m"};     
                    wlang_process_not = {L"don't", L"isn't", L"didn't", L"hasn't", L"hadn't", L"wasn't", L"weren't", L"doesn't", L"haven't", L"amn't", L"aren't", L"ain't", L"bean't", L"haven't", L"can't", L"couldn't", L"mayn't", L"mightn't", L"shan't", L"shouldn't", L"won't", L"wouldn't", L"daren't", L"mustn't", L"needn't", L"oughtn't", L"usedn't to"};     
                };
        protected:
            bool process_lang(vector<wstring> & vecwtoken) override;
    };
}

#endif // LNLP_EN_TOKENIZER_H
